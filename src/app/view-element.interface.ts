import { ViewElementTypes } from './view-element-types.enum';

export interface ViewElement {
  type?: ViewElementTypes;

  title: string;
  large: {
    left: string;
    top: string;
  };
  medium: {
    left: string;
    top: string;
  };
  zIndex?: number;
  height?: string;
  focused?: string; // focused || notFocused
  rotate?: number;

  // Image
  src?: string;

  // Title and text
  texts?: string[];

}
