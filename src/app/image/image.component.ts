import { Component, OnInit, Input } from '@angular/core';
import { ViewElement } from '../view-element.interface';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent {
  @Input() image: ViewElement;
}
