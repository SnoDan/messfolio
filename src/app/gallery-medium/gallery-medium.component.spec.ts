import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryMediumComponent } from './gallery-medium.component';

describe('GalleryMediumComponent', () => {
  let component: GalleryMediumComponent;
  let fixture: ComponentFixture<GalleryMediumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryMediumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryMediumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
