export enum FocusStates {
  focused = 'focused',
  notFocused = 'notFocused'
}
