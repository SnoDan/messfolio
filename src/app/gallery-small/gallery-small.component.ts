import { Component, OnInit, Input } from '@angular/core';
import { ViewElement } from '../view-element.interface';
import { ViewElementTypes } from '../view-element-types.enum';

@Component({
  selector: 'app-gallery-small',
  templateUrl: './gallery-small.component.html',
  styleUrls: ['./gallery-small.component.scss']
})
export class GallerySmallComponent implements OnInit {
  @Input() titles: string[];
  @Input() introductions: string[];
  @Input() abouts: string[];
  @Input() viewElements: ViewElement[];
  viewElementTypes = ViewElementTypes;

  constructor() { }

  ngOnInit(): void {
  }

}
