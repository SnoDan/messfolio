import { Component, OnInit } from '@angular/core';
import { ViewElement } from './view-element.interface';
import { state, style, transition, animate, trigger } from '@angular/animations';
import { fromEvent } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ViewElementTypes } from './view-element-types.enum';

enum Breakpoints {
  large,
  medium,
  small
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('setFocus', [
      state('notFocused', style({
        height: '500px',
      })),
      state('focused', style({
        height: '700px'
      })),
      transition('notFocused => focused', [
        animate('0.2s 0s ease-out')
      ]),
    ])
  ]
})
export class AppComponent implements OnInit {
  breakpoints = Breakpoints;
  currentBreakpoint: Breakpoints;

  viewElements: ViewElement[];

  titles = ['Pinnsvin', 'pingvin'];
  introductions = [
    `
      Lorem Ipsum er rett og slett dummytekst fra og for trykkeindustrien.
    `,
    `
      Lorem Ipsum har tålt tidens tann usedvanlig godt, og har i tillegg til å bestå gjennom fem århundrer også tålt spranget over til elektronisk typografi uten vesentlige endringer.
    `
  ];
  abouts = [
    `
      Lorem Ipsum er rett og slett dummytekst fra og for trykkeindustrien.
    `,
    `
      Lorem Ipsum har tålt tidens tann usedvanlig godt, og har i tillegg til å bestå gjennom fem århundrer også tålt spranget over til elektronisk typografi uten vesentlige endringer.
    `
  ];


  preprocessedImages: ViewElement[] = [
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%201.jpg?alt=media&token=d91cbd10-22fa-4610-9074-e0582cbd92cb',
      title: 'Side 1',
      large: {
        left: '20',
        top: '60',
      },
      medium: {
        left: '20',
        top: '60',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%202.jpg?alt=media&token=439bd868-ce5e-4118-b3ce-76f4d7608e0b',
      title: 'Side 2',
      large: {
        left: '650',
        top: '25',
      },
      medium: {
        left: '650',
        top: '25',
      },
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%203.jpg?alt=media&token=76ac621d-86d8-405b-b711-199985e6be4f',
      title: 'Side 3',
      large: {
        left: '950',
        top: '65',
      },
      medium: {
        left: '5',
        top: '950',
      },
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%204.jpg?alt=media&token=4723e6f8-0b01-4c01-a5b7-817b832327d5',
      title: 'Side 4',
      large: {
        left: '330',
        top: '270',
      },
      medium: {
        left: '330',
        top: '270',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%205.jpg?alt=media&token=bf7d1f88-b02a-4450-af8c-0b8d93e09002',
      title: 'Side 5',
      large: {
        left: '980',
        top: '440',
      },
      medium: {
        left: '400',
        top: '1150',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%206.jpg?alt=media&token=b53232d7-8fb9-4c48-a064-0dd34c83f462',
      title: 'Side 6',
      large: {
        left: '40',
        top: '520',
      },
      medium: {
        left: '40',
        top: '520',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%208.jpg?alt=media&token=4174350a-742a-4abd-8b22-b2af1cd89108',
      title: 'Side 8',
      large: {
        left: '360',
        top: '710',
      },
      medium: {
        left: '360',
        top: '710',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%209.jpg?alt=media&token=76bd6cb8-57b7-4fb8-9af5-570a8e0ed3fd',
      title: 'Side 9',
      large: {
        left: '680',
        top: '680',
      },
      medium: {
        left: '680',
        top: '680',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2010.jpg?alt=media&token=ed32cca1-8833-4542-a254-da5e833c0982',
      title: 'Side 10',
      large: {
        left: '100',
        top: '900',
      },
      medium: {
        left: '100',
        top: '1200',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2011.jpg?alt=media&token=a4ef3533-14f9-4043-afb9-a451b69c4f47',
      title: 'Side 11',
      large: {
        left: '550',
        top: '1080',
      },
      medium: {
        left: '520',
        top: '1440',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2012.jpg?alt=media&token=ce8b4a7a-9806-4287-ae31-f0f2817ce6ee',
      title: 'Side 12',
      large: {
        left: '920',
        top: '1050',
      },
      medium: {
        left: '470',
        top: '8230',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2013.jpg?alt=media&token=8b0cc6b0-bba6-4d8b-a421-8af3a9f350d0',
      title: 'Side 13',
      large: {
        left: '130',
        top: '1250',
      },
      medium: {
        left: '100',
        top: '1600',
      },
      rotate: 90
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2014.jpg?alt=media&token=37b0c56f-21db-42ec-a31c-c884bb204e95',
      title: 'Side 14',
      large: {
        left: '620',
        top: '1430',
      },
      medium: {
        left: '0',
        top: '8100',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2015.jpg?alt=media&token=c106c144-f1e8-4266-8176-4b20db4ec3c1',
      title: 'Side 15',
      large: {
        left: '820',
        top: '1430',
      },
      medium: {
        left: '620',
        top: '1720',
      },
      rotate: -90
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2016.jpg?alt=media&token=b4adc218-15ed-4b6d-bc17-ecd1deec7422',
      title: 'Side 16',
      large: {
        left: '250',
        top: '1570',
      },
      medium: {
        left: '140',
        top: '2090',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2017.jpg?alt=media&token=9663f4d4-93a0-4a6b-a7ad-13d0f8d04b88',
      title: 'Side 17',
      large: {
        left: '640',
        top: '1810',
      },
      medium: {
        left: '610',
        top: '2455',
      },
      rotate: -90
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2018.jpg?alt=media&token=72c3f1a5-8104-4115-8ef2-b14995f96a12',
      title: 'Side 18',
      large: {
        left: '945',
        top: '1770',
      },
      medium: {
        left: '460',
        top: '2080',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2019.jpg?alt=media&token=6545d187-bdac-4a5f-a8e2-863493d0c94f',
      title: 'Side 19',
      large: {
        left: '400',
        top: '2140',
      },
      medium: {
        left: '20',
        top: '2480',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2020.jpg?alt=media&token=26a240a5-eb0d-4656-8705-08536db96d68',
      title: 'Side 20',
      large: {
        left: '900',
        top: '2160',
      },
      medium: {
        left: '320',
        top: '2680',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2021.jpg?alt=media&token=80061f36-7a5d-4842-83ab-31ae29f40398',
      title: 'Side 21',
      large: {
        left: '30',
        top: '1930',
      },
      medium: {
        left: '680',
        top: '2900',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2022.jpg?alt=media&token=5d5595a2-5cf9-4fed-affb-7194e14f82c2',
      title: 'Side 22',
      large: {
        left: '80',
        top: '2270',
      },
      medium: {
        left: '50',
        top: '3050',
      },
      rotate: 90
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2023.jpg?alt=media&token=0844c419-9e76-4e80-a645-73dcb1747eed',
      title: 'Side 23',
      large: {
        left: '600',
        top: '2310',
      },
      medium: {
        left: '570',
        top: '3300',
      },
      rotate: -90
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2024.jpg?alt=media&token=2d102fb0-921b-4085-8395-45ba434422a5',
      title: 'Side 24',
      large: {
        left: '310',
        top: '2490',
      },
      medium: {
        left: '50',
        top: '3500',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2025.jpg?alt=media&token=bbff1edd-7d81-4f31-8ef7-ad94bb4c0e8d',
      title: 'Side 25',
      large: {
        left: '810',
        top: '2520',
      },
      medium: {
        left: '300',
        top: '3700',
      },
      rotate: -90
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2026.jpg?alt=media&token=98b080ea-51f3-43f3-a1b9-d19c1fb7d0bb',
      title: 'Side 26',
      large: {
        left: '10',
        top: '2730',
      },
      medium: {
        left: '670',
        top: '3610',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2027.jpg?alt=media&token=d45cb70d-fbb1-4f68-a40a-67c9b7b48fe4',
      title: 'Side 27',
      large: {
        left: '480',
        top: '2890',
      },
      medium: {
        left: '0',
        top: '4000',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2028.jpg?alt=media&token=f1dfc12e-d01d-4679-9bdc-0b4701140091',
      title: 'Side 28',
      large: {
        left: '880',
        top: '2960',
      },
      medium: {
        left: '630',
        top: '4100',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2029.jpg?alt=media&token=51f5a4fb-8246-4421-9e6b-19b466e8f4d4',
      title: 'Side 29',
      large: {
        left: '230',
        top: '3100',
      },
      medium: {
        left: '300',
        top: '4100',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2030.jpg?alt=media&token=cd8ff3b0-4741-462a-87a7-7738dd09e267',
      title: 'Side 30',
      large: {
        left: '620',
        top: '3180',
      },
      medium: {
        left: '200',
        top: '4370',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2031.jpg?alt=media&token=80fb2463-be81-483e-a98d-c7e5e699eddd',
      title: 'Side 31',
      large: {
        left: '0',
        top: '3150',
      },
      medium: {
        left: '560',
        top: '4550',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2032.jpg?alt=media&token=3886abcb-9d76-46b8-b6c1-2bc09aa73dd5',
      title: 'Side 32',
      large: {
        left: '20',
        top: '3550',
      },
      medium: {
        left: '60',
        top: '4700',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2033.jpg?alt=media&token=bb0e7da2-41d2-4893-a8f6-b4083fd554e1',
      title: 'Side 33',
      large: {
        left: '360',
        top: '3575',
      },
      medium: {
        left: '0',
        top: '5200',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2034.jpg?alt=media&token=86965815-f73e-479d-b040-1277331f6c32',
      title: 'Side 34',
      large: {
        left: '630',
        top: '3560',
      },
      medium: {
        left: '650',
        top: '5000',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2035.jpg?alt=media&token=096cd7fb-39b9-4ce6-807e-4db5d0bea75c',
      title: 'Side 35',
      large: {
        left: '850',
        top: '3760',
      },
      medium: {
        left: '360',
        top: '5100',
      },
      rotate: -90
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2036.jpg?alt=media&token=c15a95cd-d459-4ab4-88c5-3ff670694014',
      title: 'Side 36',
      large: {
        left: '220',
        top: '3920',
      },
      medium: {
        left: '650',
        top: '5500',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2037.jpg?alt=media&token=03a89ccf-1843-4783-aa0b-860171d7b28c',
      title: 'Side 37',
      large: {
        left: '600',
        top: '4100',
      },
      medium: {
        left: '200',
        top: '5650',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2038.jpg?alt=media&token=cae4c18c-45e2-4a6f-b828-b0dba8f4ad9b',
      title: 'Side 38',
      large: {
        left: '840',
        top: '4200',
      },
      medium: {
        left: '620',
        top: '5900',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2039.jpg?alt=media&token=9c1e9f9c-e220-475e-aaab-cb8318fedb79',
      title: 'Side 39',
      large: {
        left: '30',
        top: '4300',
      },
      medium: {
        left: '30',
        top: '6100',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2040.jpg?alt=media&token=bef8cdc1-56a2-4998-b198-8b1dea123bfc',
      title: 'Side 40',
      large: {
        left: '350',
        top: '4400',
      },
      medium: {
        left: '400',
        top: '6300',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2041.jpg?alt=media&token=a5f0c7a9-ab07-4764-baf1-1eaa4346063b',
      title: 'Side 41',
      large: {
        left: '720',
        top: '4600',
      },
      medium: {
        left: '0',
        top: '6550',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2042.jpg?alt=media&token=6c4e417a-1a42-4176-b32a-43f221cae0a4',
      title: 'Side 42',
      large: {
        left: '50',
        top: '4750',
      },
      medium: {
        left: '640',
        top: '6720',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2043.jpg?alt=media&token=de37fabc-9b91-4218-90d4-fb55f3ebabf3',
      title: 'Side 43',
      large: {
        left: '420',
        top: '4880',
      },
      medium: {
        left: '60',
        top: '6850',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2044.jpg?alt=media&token=0f8c78ec-94cb-4614-8115-ab0f47871167',
      title: 'Side 44',
      large: {
        left: '860',
        top: '5000',
      },
      medium: {
        left: '230',
        top: '7100',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2045.jpg?alt=media&token=e27d31e3-377d-43a9-a9ba-8374567f9d86',
      title: 'Side 45',
      large: {
        left: '0',
        top: '5170',
      },
      medium: {
        left: '550',
        top: '7300',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2046.jpg?alt=media&token=2b889c0b-76fd-46d4-b9fc-3c4e1f5bd8a2',
      title: 'Side 46',
      large: {
        left: '200',
        top: '5380',
      },
      medium: {
        left: '0',
        top: '7600',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2047.jpg?alt=media&token=e26173bd-6f4a-4bdf-887c-b83052344fbb',
      title: 'Side 47',
      large: {
        left: '530',
        top: '5250',
      },
      medium: {
        left: '650',
        top: '7800',
      }
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/messfolio-78087.appspot.com/o/Side%2048.jpg?alt=media&token=7a079c94-1da7-4ade-9950-84f91d737bc2',
      title: 'Side 48',
      large: {
        left: '820',
        top: '5400',
      },
      medium: {
        left: '320',
        top: '7850',
      }
    },

  ];

  ngOnInit() {
    this.viewElements = this.preprocessedImages.map((image, index) => {
      return {...image, type: ViewElementTypes.Image, focused: 'notFocused'};
    });

    fromEvent(window, 'resize').pipe(
      map(event => (event.target as any).innerWidth),
      startWith(window.innerWidth)
    ).subscribe(innerWidth => {
      if (innerWidth > 1280) {
        this.currentBreakpoint = Breakpoints.large;
      } else if (innerWidth <= 1280 && innerWidth > 960) {
        this.currentBreakpoint = Breakpoints.medium;
      } else {
        this.currentBreakpoint = Breakpoints.small;
      }
    });

  }

}
