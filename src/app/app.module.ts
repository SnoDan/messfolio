import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ImageComponent } from './image/image.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MobileComponent } from './mobile/mobile.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GalleryLargeComponent } from './gallery-large/gallery-large.component';
import { GalleryMediumComponent } from './gallery-medium/gallery-medium.component';
import { GallerySmallComponent } from './gallery-small/gallery-small.component';

@NgModule({
  declarations: [
    AppComponent,
    ImageComponent,
    MobileComponent,
    GalleryLargeComponent,
    GalleryMediumComponent,
    GallerySmallComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DragDropModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
