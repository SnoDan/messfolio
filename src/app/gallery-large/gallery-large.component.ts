import { Component, OnInit, Input } from '@angular/core';
import { ViewElement } from '../view-element.interface';
import { ViewElementTypes } from '../view-element-types.enum';
import { state, style, trigger, transition, animate } from '@angular/animations';
import { FocusStates } from '../focus-states.enum';

@Component({
  selector: 'app-gallery-large',
  templateUrl: './gallery-large.component.html',
  styleUrls: ['./gallery-large.component.scss'],
  animations: [
    trigger('focusing', [
      state('focused', style({
        width: '430px'
      })),
      state('notFocused', style({
        width: '280px',
      })),
      transition('notFocused => focused', [
        animate('0.6s 0ms ease-in-out')
      ]),
      transition('focused => notFocused', [
        animate('0.6s 0ms ease-in-out')
      ]),
    ])
  ]
})
export class GalleryLargeComponent implements OnInit {
  @Input() titles: string[];
  @Input() introductions: string[];
  @Input() abouts: string[];

  @Input() viewElements: ViewElement[];
  desktopViewElements: ViewElement[];
  topZIndex = 1;

  viewElementTypes = ViewElementTypes;

  ngOnInit(): void {

    this.desktopViewElements = this.viewElements.map((image, index) => {
      return { ...image, zIndex: this.topZIndex + index, height: '500px'};
    });

    this.topZIndex = this.topZIndex + this.viewElements.length;
  }

  setFocusedElement(element: ViewElement) {
    if (element.focused === FocusStates.focused) {
      element.focused = FocusStates.notFocused;
      return;
    }

    this.desktopViewElements.forEach(e => {
      if (e.focused === FocusStates.focused) {
        e.focused = FocusStates.notFocused;
      }
    });
    element.focused = FocusStates.focused;
  }
}
